#pragma once
#include <iostream>

class Punct2D
{
	friend Punct2D duplicate(Punct2D obj);
private:
	double m_x = 0.0;
	double m_y = 0.0;
	int m_size = 0;
	int* m_array = nullptr;
public:
	Punct2D(); //default constructor
	Punct2D(double, double, int size); //constructor
	void print(); //method
	~Punct2D(); //destroyer
	Punct2D(Punct2D& obj); //copy constructor
	Punct2D(Punct2D&& obj); //move constructor

};