#include <iostream>
#include "EucledianDist.h"
Punct2D duplicate(Punct2D obj)
{
	return Punct2D(obj.m_x, obj.m_y, obj.m_size);
}

int main()
{
	//cpycstr Punct2D B(5.0, 7.0, 20); 
	//cpycstrPunct2D A(B);
	//cpycstrA.print();
	//cpycstrB.print();
	//Punct2D C = duplicate(A);
	//C.print();
	 Punct2D B(5.0, 7.0, 20); 
	 B.print();
	 Punct2D A(std::move(B));
	 A.print();
	 
}