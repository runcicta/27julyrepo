#include <math.h>
#include "EucledianDist.h"

Punct2D::Punct2D() //default constructor
{

}

Punct2D::Punct2D(double x, double y, int size) //constructor
{
	this -> m_x = x;
	this -> m_y = y;
	this->m_size = size;
	m_array = new int[size];
}
void Punct2D::print() //method
{
	std::cout << "(" << m_x << "," <<  m_y << ")" <<std::endl;
	std::cout << "\t" << m_size << " allocated at " << m_array << std::endl;
}

Punct2D::~Punct2D() //destroyer
{
	delete[] m_array;
}

Punct2D::Punct2D(Punct2D& obj) //copy constructor
{
	m_x = obj.m_x;
	m_y = obj.m_y;
	m_size = obj.m_size;
	m_array = new int[m_size];

}

Punct2D::Punct2D(Punct2D&& obj) //move constructor
{
	
	m_x = obj.m_x;
	m_y = obj.m_y;
	m_size = obj.m_size;
	this->m_array = obj.m_array;
	obj.m_array = nullptr;
	obj.m_size = 0;

}

