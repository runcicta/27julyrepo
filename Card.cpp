#include "Card.h"

Card::Card()
{
	m_rank = Ace;
	m_suit = HEARTS;
}

Card::Card(Rank rank, Suits suit)
{
	m_rank = rank;
	m_suit = suit;
}

void Card::printCard() const
{	
	if (m_rank < 10)
	{
		std::cout << m_rank << getSuit() <<std::endl;
	}
	else
	{
		std::cout << getFigureCard() << getSuit() << std::endl;
	}
	 
}

int Card::getCardValue() const
{
	switch (m_rank)
	{
	case Rank::Jack:
		return 10;
	case Rank::Queen:
		return 10;
	case Rank::King:
		return 10;
	case Rank::Ace:
		return 11;
	default:
		return m_rank;
	}

	
}

char Card::getSuit() const
{
	char suitchar;
	switch (m_suit)
	{
	case Suits::HEARTS:
		suitchar = 'H';
		break;
	case Suits::SPADES:
		suitchar = 'S';
		break;
	case Suits::DIAMOND:
		suitchar = 'D';
		break;
	case Suits::CLUB:
		suitchar = 'C';
		break;
	default:
		break;
	}
	return suitchar;

}

char Card::getFigureCard() const
{
	char figurechar;
	switch (m_rank)
	{
	case Rank::Ace:
		figurechar = 'A';
		break;
	case Rank::Jack:
		figurechar = 'J';
		break;
	case Rank::Queen:
		figurechar = 'Q';
		break;
	case Rank::King:
		figurechar = 'K';
		break;
	default:
		break;
	}
	return figurechar;
}