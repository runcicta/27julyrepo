#pragma once
#include <iostream>

class Card
{

public:
	enum Rank
	{
		Two = 2,
		Three,
		Four,
		Five,
		Six,
		Seven,
		Eight,
		Nine,
		Ten,
		Jack,
		Queen,
		King,
		Ace,
	};
	enum Suits
	{
		HEARTS = 1,
		SPADES,
		DIAMOND,
		CLUB,
	};

public:
	Card();
	Card(Rank rank, Suits suit);
	void printCard() const;
	int getCardValue() const;
	char getSuit() const;
	char getFigureCard() const;

private:
	Rank m_rank;
	Suits m_suit;
};