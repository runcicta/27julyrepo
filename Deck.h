#pragma once
#include <iostream>
#include "Card.h"
#include <array>

class Deck
{
public:
	Deck();
	void printDeck();
	void swapCard(const unsigned& i, const unsigned& j);
	void shuffleDeck();

private:
	std::array<Card, 52> m_deck;
};