#include <iostream>
#include "Card.h"

int main()
{
	const Card cardQueenHearts(Card::Rank::Queen, Card::Suits::HEARTS);
	cardQueenHearts.printCard();
	std::cout << "has the value " << cardQueenHearts.getCardValue() << std::endl;

	return 0;
}