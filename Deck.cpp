#include "Deck.h"
#include "Card.h"
#include <random>

Deck::Deck()
{
	int cardPopulateIndex = 0;
	for (int i = 2; i < static_cast<int>(Card::RankEnd); i++)
	{
		for (int j = 1; i < static_cast<int>(Card::SuitEnd); j++)
		{
			m_deck[cardPopulateIndex] = Card(static_cast<Card::Rank>(i), static_cast<Card::Suits>(j));
			cardPopulateIndex++;
		}
	}
}

void Deck::printDeck()
{
	for (int i = 0; i < 52; i++)
	{
		std::cout << m_deck[i].getCardValue() << " " << std::endl;
	}
}

void Deck::swapCard(const unsigned& i, const unsigned& j)
{
	Card aux = m_deck[i];
	m_deck[i] = m_deck[j];
	m_deck[j] = aux;
}

void Deck::shuffleDeck()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 51);
	for (int index = 0; index < m_deck.size(); index++)
	{
		int swapIndex = dis(gen);
		swapCard(swapIndex, index);
	}
}