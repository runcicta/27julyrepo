#include <iostream>
#include "Card.h"
#include "Deck.h"

int main()
{
	const Card cardQueenHearts(Card::Rank::Ace, Card::Suits::HEARTS);
	cardQueenHearts.printCard();
	std::cout << "has the value " << cardQueenHearts.getCardValue() << std::endl;
	Deck A;
	A.printDeck();
	A.shuffleDeck();
	A.printDeck();

	return 0;
}